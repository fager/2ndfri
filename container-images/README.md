# Container-Images

## Erstellen der Images

Für jedes Container-Image gibt es in diesem Verzeichnis einen Dockerfile,
mit dem das Container-Image erstellt wird.

Alle verfügbaren Container-Images können wie folgt erstellt werden.

```
./build.sh all
```

Die verfügbaren Container-Images sind an der Dateiendung der Dockerfiles
zu erkennen.

Aus der Datei ```Dockerfile.deb11``` wird ein Container-Image mit dem
Tag ```2ndFri:deb11``` erzeugt.

Der Name des Images kann an das Script mit übergeben werden, um nur dieses
eine Container-Image neu zu erstellen:

```
./build.sh deb11
```

## Starten einer Bash in einem Image

Der Name des Container-Images kann auch an das Script ```run.sh``` übergeben
werden, um einen Container mit dem Image zu starten und darin eine Bash aufzurufen:

```
./run.sh deb11
```


#!/bin/bash

if [ "${#}" != 1 ]
then
  echo "usage:"
  echo "build.sh <image-name>"
  exit 1
fi

_image="${1}"

if [ "${_image}" = "all" ]
then
  for f in Dockerfile.*
  do
    $0 "${f#Dockerfile.}"
  done
  exit 0
fi


if which podman
then
  DOCKER=podman
elif which docker
then
  DOCKER=docker
fi

if [ -f "Dockerfile.${1}" ]
then
  $DOCKER build -t "2ndfri:${_image}" -f "Dockerfile.${_image}" .
else
  echo "No Dockerfile found"
  exit 1
fi




# 2. Freitag im Monat (2ndfri)

Der zweite Freitag im Monat wird von der (LUG Flensburg)[https://www.lugfl.de]
für ein eher technisches Vortrags/Workshop-Programm verwendet.

In diesem Repository befinden sich Notizen und Scripte dazu...

## Container Images

Für einige Demos können Container-Images erstellt werden.

Docker oder Podman müssen dafür auf dem Linux-System installiert und
funktionsfähig sein.

Das Script zum Erstellen der Container-Images und zum Starten einer
Bash in einem solchen Container sind im Verzeichnis
[container-images](./container-images/) enthalten.

## Themen

- 08.10.2021 - Debian Packete selber bauen

